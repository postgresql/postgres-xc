#!/bin/sh

set -e

SSL_ROOT=/etc/postgres-xc/root.crt
PGXC_DATA_ROOT=/var/lib/postgres-xc
PGXC_GTM_ROOT=/etc/postgres-xc/gtm
PGXC_GTMPROXY_ROOT=/etc/postgres-xc/gtm_proxy
PGXC_COORDINATOR_ROOT=/etc/postgres-xc/coordinator
PGXC_DATANODE_ROOT=/etc/postgres-xc/datanode

move_and_link()
{
	mv $PGXC_DATA_ROOT/$2/$3 $1/$2
	ln -s $1/$2/$3 $PGXC_DATA_ROOT/$2
}

move_postgresql_conf()
{
	if [ $1 = 'coordinator' ]; then
		dir=$PGXC_COORDINATOR_ROOT
		port_argument="#"
	else
		dir=$PGXC_DATANODE_ROOT
		port_argument="s/#port = 5432/port = $1/"
	fi

	sed -e "$port_argument" \
	    -e "s%#data_directory = 'ConfigDir'%data_directory = '$PGXC_DATA_ROOT/$2'%" \
	    -e "s%#hba_file = 'ConfigDir/pg_hba.conf'%hba_file = '$dir/$2/pg_hba.conf'%" \
	    -e "s%#ident_file = 'ConfigDir/pg_ident.conf'%ident_file = '$dir/$2/pg_ident.conf'%" \
	    -e "s%#external_pid_file = '(none)'%external_pid_file = '/var/run/postgresql/$2.pid'%" \
	< $PGXC_DATA_ROOT/$2/postgresql.conf > $dir/$2/postgresql.conf
	rm -f $PGXC_DATA_ROOT/$2/postgresql.conf
	ln -s $dir/$2/postgresql.conf $PGXC_DATA_ROOT/$2
}

if [ "$1" = configure ]; then
    # Make sure the administrative user exists
    if ! getent passwd postgres-xc > /dev/null; then
        adduser --system --quiet --home $PGXC_DATA_ROOT --no-create-home \
            --shell /bin/bash --group --gecos "Postgres-XC administrator" postgres-xc
    fi

    # check validity of postgres user and group
    if [ "`id -u postgres-xc`" -eq 0 ]; then
        echo "The postgres system user must not have uid 0 (root).
Please fix this and reinstall this package." >&2
        exit 1
    fi

    if [ "`id -g postgres-xc`" -eq 0 ]; then
        echo "The postgres-xc system user must not have root as primary group.
Please fix this and reinstall this package." >&2
        exit 1
    fi

    # ensure home directory ownership
    mkdir -p $PGXC_DATA_ROOT
    chown postgres-xc:postgres-xc $PGXC_DATA_ROOT

    # nicer log directory permissions
    mkdir -p /var/log/postgres-xc
    chmod 1775 /var/log/postgres-xc
    chown root:postgres-xc /var/log/postgres-xc

    # create default dummy root.crt if not present
    if ! [ -e "$SSL_ROOT" ]; then
        cat > "$SSL_ROOT" <<EOF
This is a dummy root certificate file for Postgres-XC. To enable client side
authentication, add some certificates to it. Client certificates must be signed
with any certificate in this file to be accepted. 

A reasonable choice is to just symlink this file to
/etc/ssl/certs/ssl-cert-snakeoil.pem; in this case, client certificates need to
be signed by the postgres-xc server certificate, which might be desirable in
many cases. See

  file:///usr/share/doc/postgres-xc-doc/html/ssl-tcp.html

for details (in package postgres-xc-doc).
EOF
    fi

    if [ -z "$2" ]; then
    	# Add postgres-xc user to the ssl-cert group on fresh installs
        if getent group ssl-cert >/dev/null; then
            adduser --quiet postgres-xc ssl-cert
        fi

	# if no database cluster exists yet, so we create one
	cd $PGXC_DATA_ROOT
	if [ ! -d  GTM ]; then	
		# Initialize GTM
		su - postgres-xc -c "initgtm -Z gtm -D GTM"
		mkdir $PGXC_GTM_ROOT/GTM
		move_and_link $PGXC_GTM_ROOT GTM gtm.conf
		touch $PGXC_GTM_ROOT/GTM/run

		# Initialize GTM_PROXY
		su - postgres-xc -c "initgtm -Z gtm_proxy -D GTM_PROXY"
		mkdir $PGXC_GTMPROXY_ROOT/GTM_PROXY
		move_and_link $PGXC_GTMPROXY_ROOT GTM_PROXY gtm_proxy.conf
		touch $PGXC_GTMPROXY_ROOT/GTM_PROXY/run

		# Initialize Coordinator
		su - postgres-xc -c "initdb -D CN --nodename cn"
		mkdir $PGXC_COORDINATOR_ROOT/CN
		move_postgresql_conf coordinator CN
		move_and_link $PGXC_COORDINATOR_ROOT CN pg_hba.conf
		move_and_link $PGXC_COORDINATOR_ROOT CN pg_ident.conf
		touch $PGXC_COORDINATOR_ROOT/CN/run

		# Initialize Datanodes
		mkdir $PGXC_DATANODE_ROOT/DN1
		su - postgres-xc -c "initdb -D DN1 --nodename dn1"
		move_postgresql_conf 15432 DN1
		move_and_link $PGXC_DATANODE_ROOT DN1 pg_hba.conf
		move_and_link $PGXC_DATANODE_ROOT DN1 pg_ident.conf
		touch $PGXC_DATANODE_ROOT/DN1/run

		mkdir $PGXC_DATANODE_ROOT/DN2
		su - postgres-xc -c "initdb -D DN2 --nodename dn2"
		move_postgresql_conf 15433 DN2
		move_and_link $PGXC_DATANODE_ROOT DN2 pg_hba.conf
		move_and_link $PGXC_DATANODE_ROOT DN2 pg_ident.conf
		touch $PGXC_DATANODE_ROOT/DN2/run
	fi
   fi
fi

#DEBHELPER#

if [ "$1" = configure -a -z "$2" ]; then
	# wait until server is up and running
	i=1
	while [ ! -S /var/run/postgresql/.s.PGSQL.5432 ]; do
		i=$((i+1))
		sleep 1
		[ $i -gt 10 ] && echo "
		Server cannot be started! 
		Please run CREATE NODE command manually after starting it!
		" && exit 0
	done

	# finish cluster installation for fresh install
	su - postgres-xc -c '
		psql postgres -c "
			CREATE NODE dn1 WITH (TYPE='datanode', PORT=15432); 
			CREATE NODE dn2 WITH (TYPE='datanode', PORT=15433); 
			select pgxc_pool_reload();
		"
		'
fi

# Environment
su - postgres-xc <<-EOF
if [ -z "\$PGDATABASE" ]; then
	echo 'export PGDATABASE=postgres' >> .profile
else
	:
fi
EOF

