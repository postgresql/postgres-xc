'\" t
.\"     Title: SET TRANSACTION
.\"    Author: The Postgres-XC Development Group
.\" Generator: DocBook XSL Stylesheets v1.75.2 <http://docbook.sf.net/>
.\"      Date: 2014
.\"    Manual: Postgres-XC 1.2.1 Documentation
.\"    Source: Postgres-XC 1.2.1
.\"  Language: English
.\"
.TH "SET TRANSACTION" "7" "2014" "Postgres-XC 1.2.1" "Postgres-XC 1.2.1 Documentation"
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
SET_TRANSACTION \- set the characteristics of the current transaction
.\" SET TRANSACTION
.\" transaction isolation level: setting
.\" read-only transaction: setting
.\" deferrable transaction: setting
.SH "SYNOPSIS"
.sp
.nf
SET TRANSACTION \fItransaction_mode\fR [, \&.\&.\&.]
SET TRANSACTION SNAPSHOT \fIsnapshot_id\fR
SET SESSION CHARACTERISTICS AS TRANSACTION \fItransaction_mode\fR [, \&.\&.\&.]

where \fItransaction_mode\fR is one of:

    ISOLATION LEVEL { SERIALIZABLE | REPEATABLE READ | READ COMMITTED | READ UNCOMMITTED }
    READ WRITE | READ ONLY
    [ NOT ] DEFERRABLE
.fi
.SH "DESCRIPTION"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
The
SET TRANSACTION
command sets the characteristics of the current transaction\&. It has no effect on any subsequent transactions\&.
SET SESSION CHARACTERISTICS
sets the default transaction characteristics for subsequent transactions of a session\&. These defaults can be overridden by
SET TRANSACTION
for an individual transaction\&.
.PP
The available transaction characteristics are the transaction isolation level, the transaction access mode (read/write or read\-only), and the deferrable mode\&. In addition, a snapshot can be selected, though only for the current transaction, not as a session default\&.
.PP
The isolation level of a transaction determines what data the transaction can see when other transactions are running concurrently:
.PP
READ COMMITTED
.RS 4
A statement can only see rows committed before it began\&. This is the default\&.
.RE
.PP
REPEATABLE READ
.RS 4
All statements of the current transaction can only see rows committed before the first query or data\-modification statement was executed in this transaction\&.
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
\fIXCONLY\fR: The following description applies only to
Postgres\-XC\&.
.sp .5v
.RE
REPEATABLE READ
does not run properly with current version of
Postgres\-XC
.RE
.PP
SERIALIZABLE
.RS 4
All statements of the current transaction can only see rows committed before the first query or data\-modification statement was executed in this transaction\&. If a pattern of reads and writes among concurrent serializable transactions would create a situation which could not have occurred for any serial (one\-at\-a\-time) execution of those transactions, one of them will be rolled back with a
serialization_failure
error\&.
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
\fIXCONLY\fR: The following description applies only to
Postgres\-XC\&.
.sp .5v
.RE
SERIALIZABLE
does not run properly with current version of
Postgres\-XC
.RE
The SQL standard defines one additional level,
READ UNCOMMITTED\&. In
PostgreSQLREAD UNCOMMITTED
is treated as
READ COMMITTED\&.
.PP
The transaction isolation level cannot be changed after the first query or data\-modification statement (SELECT,
INSERT,
DELETE,
UPDATE,
FETCH, or
COPY) of a transaction has been executed\&. See
Chapter 13, Concurrency Control, in the documentation
for more information about transaction isolation and concurrency control\&.
.PP
The transaction access mode determines whether the transaction is read/write or read\-only\&. Read/write is the default\&. When a transaction is read\-only, the following SQL commands are disallowed:
INSERT,
UPDATE,
DELETE, and
COPY FROM
if the table they would write to is not a temporary table; all
CREATE,
ALTER, and
DROP
commands;
COMMENT,
GRANT,
REVOKE,
TRUNCATE; and
EXPLAIN ANALYZE
and
EXECUTE
if the command they would execute is among those listed\&. This is a high\-level notion of read\-only that does not prevent all writes to disk\&.
.PP
The
DEFERRABLE
transaction property has no effect unless the transaction is also
SERIALIZABLE
and
READ ONLY\&. When all three of these properties are selected for a transaction, the transaction may block when first acquiring its snapshot, after which it is able to run without the normal overhead of a
SERIALIZABLE
transaction and without any risk of contributing to or being canceled by a serialization failure\&. This mode is well suited for long\-running reports or backups\&.
.PP
The
SET TRANSACTION SNAPSHOT
command allows a new transaction to run with the same
snapshot
as an existing transaction\&. The pre\-existing transaction must have exported its snapshot with the
pg_export_snapshot
function (see
Section 9.26.5, \(lqSnapshot Synchronization Functions\(rq, in the documentation)\&. That function returns a snapshot identifier, which must be given to
SET TRANSACTION SNAPSHOT
to specify which snapshot is to be imported\&. The identifier must be written as a string literal in this command, for example
\(aq000003A1\-1\(aq\&.
SET TRANSACTION SNAPSHOT
can only be executed at the start of a transaction, before the first query or data\-modification statement (SELECT,
INSERT,
DELETE,
UPDATE,
FETCH, or
COPY) of the transaction\&. Furthermore, the transaction must already be set to
SERIALIZABLE
or
REPEATABLE READ
isolation level (otherwise, the snapshot would be discarded immediately, since
READ COMMITTED
mode takes a new snapshot for each command)\&. If the importing transaction uses
SERIALIZABLE
isolation level, then the transaction that exported the snapshot must also use that isolation level\&. Also, a non\-read\-only serializable transaction cannot import a snapshot from a read\-only transaction\&.
.SH "NOTES"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
If
SET TRANSACTION
is executed without a prior
START TRANSACTION
or
BEGIN, it will appear to have no effect, since the transaction will immediately end\&.
.PP
It is possible to dispense with
SET TRANSACTION
by instead specifying the desired
\fItransaction_modes\fR
in
BEGIN
or
START TRANSACTION\&. But that option is not available for
SET TRANSACTION SNAPSHOT\&.
.PP
The session default transaction modes can also be set by setting the configuration parameters
default_transaction_isolation,
default_transaction_read_only, and
default_transaction_deferrable\&. (In fact
SET SESSION CHARACTERISTICS
is just a verbose equivalent for setting these variables with
SET\&.) This means the defaults can be set in the configuration file, via
ALTER DATABASE, etc\&. Consult
Chapter 17, Coordinator and Datanode Configuration, in the documentation
for more information\&.
.SH "EXAMPLES"
.PP
To begin a new transaction with the same snapshot as an already existing transaction, first export the snapshot from the existing transaction\&. That will return the snapshot identifier, for example:
.sp
.if n \{\
.RS 4
.\}
.nf
BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SELECT pg_export_snapshot();
 pg_export_snapshot
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
 000003A1\-1
(1 row)
.fi
.if n \{\
.RE
.\}
.sp
Then give the snapshot identifier in a
SET TRANSACTION SNAPSHOT
command at the beginning of the newly opened transaction:
.sp
.if n \{\
.RS 4
.\}
.nf
BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SET TRANSACTION SNAPSHOT \(aq000003A1\-1\(aq;
.fi
.if n \{\
.RE
.\}
.SH "COMPATIBILITY"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
These commands are defined in the
SQL
standard, except for the
DEFERRABLE
transaction mode and the
SET TRANSACTION SNAPSHOT
form, which are
PostgreSQL
extensions\&.
.PP
SERIALIZABLE
is the default transaction isolation level in the standard\&. In
PostgreSQL
the default is ordinarily
READ COMMITTED, but you can change it as mentioned above\&.
.PP
In the SQL standard, there is one other transaction characteristic that can be set with these commands: the size of the diagnostics area\&. This concept is specific to embedded SQL, and therefore is not implemented in the
PostgreSQL
server\&.
.PP
The SQL standard requires commas between successive
\fItransaction_modes\fR, but for historical reasons
PostgreSQL
allows the commas to be omitted\&.
