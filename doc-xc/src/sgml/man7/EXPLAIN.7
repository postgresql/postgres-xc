'\" t
.\"     Title: EXPLAIN
.\"    Author: The Postgres-XC Development Group
.\" Generator: DocBook XSL Stylesheets v1.75.2 <http://docbook.sf.net/>
.\"      Date: 2014
.\"    Manual: Postgres-XC 1.2.1 Documentation
.\"    Source: Postgres-XC 1.2.1
.\"  Language: English
.\"
.TH "EXPLAIN" "7" "2014" "Postgres-XC 1.2.1" "Postgres-XC 1.2.1 Documentation"
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
EXPLAIN \- show the execution plan of a statement
.\" EXPLAIN
.\" prepared statements: showing the query plan
.\" cursor: showing the query plan
.SH "SYNOPSIS"
.sp
.nf
EXPLAIN [ ( \fIoption\fR [, \&.\&.\&.] ) ] \fIstatement\fR
EXPLAIN [ ANALYZE ] [ VERBOSE ] \fIstatement\fR

where \fIoption\fR can be one of:

    ANALYZE [ \fIboolean\fR ]
    VERBOSE [ \fIboolean\fR ]
    COSTS [ \fIboolean\fR ]
    BUFFERS [ \fIboolean\fR ]
    TIMING [ \fIboolean\fR ]
    FORMAT { TEXT | XML | JSON | YAML }
.fi
.SH "DESCRIPTION"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
This command displays the execution plan that the
PostgreSQL
planner generates for the supplied statement\&. The execution plan shows how the table(s) referenced by the statement will be scanned \(em by plain sequential scan, index scan, etc\&. \(em and if multiple tables are referenced, what join algorithms will be used to bring together the required rows from each input table\&.
.PP
The most critical part of the display is the estimated statement execution cost, which is the planner\(aqs guess at how long it will take to run the statement (measured in cost units that are arbitrary, but conventionally mean disk page fetches)\&. Actually two numbers are shown: the start\-up cost before the first row can be returned, and the total cost to return all the rows\&. For most queries the total cost is what matters, but in contexts such as a subquery in
EXISTS, the planner will choose the smallest start\-up cost instead of the smallest total cost (since the executor will stop after getting one row, anyway)\&. Also, if you limit the number of rows to return with a
LIMIT
clause, the planner makes an appropriate interpolation between the endpoint costs to estimate which plan is really the cheapest\&.
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
\fIXCONLY\fR: The following description applies only to
Postgres\-XC\&.
.sp .5v
.RE
.PP
Please note that
explain
explains local plan at the Coordinator only\&. If you\(aqd like to see remote plan at Datanodes, use
auto_explain
package at
auto_explain\&.
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
The
ANALYZE
option causes the statement to be actually executed, not only planned\&. Then actual run time statistics are added to the display, including the total elapsed time expended within each plan node (in milliseconds) and the total number of rows it actually returned\&. This is useful for seeing whether the planner\(aqs estimates are close to reality\&.
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBImportant\fR
.ps -1
.br
.PP
Keep in mind that the statement is actually executed when the
ANALYZE
option is used\&. Although
EXPLAIN
will discard any output that a
SELECT
would return, other side effects of the statement will happen as usual\&. If you wish to use
EXPLAIN ANALYZE
on an
INSERT,
UPDATE,
DELETE,
CREATE TABLE AS, or
EXECUTE
statement without letting the command affect your data, use this approach:
.sp
.if n \{\
.RS 4
.\}
.nf
BEGIN;
EXPLAIN ANALYZE \&.\&.\&.;
ROLLBACK;
.fi
.if n \{\
.RE
.\}
.sp .5v
.RE
.PP
Only the
ANALYZE
and
VERBOSE
options can be specified, and only in that order, without surrounding the option list in parentheses\&. Prior to
PostgreSQL
9\&.0, the unparenthesized syntax was the only one supported\&. It is expected that all new options will be supported only in the parenthesized syntax\&.
.SH "PARAMETERS"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
ANALYZE
.RS 4
Carry out the command and show actual run times and other statistics\&. This parameter defaults to
FALSE\&.
.RE
.PP
VERBOSE
.RS 4
Display additional information regarding the plan\&. Specifically, include the output column list for each node in the plan tree, schema\-qualify table and function names, always label variables in expressions with their range table alias, and always print the name of each trigger for which statistics are displayed\&. This parameter defaults to
FALSE\&.
.RE
.PP
COSTS
.RS 4
Include information on the estimated startup and total cost of each plan node, as well as the estimated number of rows and the estimated width of each row\&. This parameter defaults to
TRUE\&.
.RE
.PP
BUFFERS
.RS 4
Include information on buffer usage\&. Specifically, include the number of shared blocks hit, read, dirtied, and written, the number of local blocks hit, read, dirtied, and written, and the number of temp blocks read and written\&. A
\fIhit\fR
means that a read was avoided because the block was found already in cache when needed\&. Shared blocks contain data from regular tables and indexes; local blocks contain data from temporary tables and indexes; while temp blocks contain short\-term working data used in sorts, hashes, Materialize plan nodes, and similar cases\&. The number of blocks
\fIdirtied\fR
indicates the number of previously unmodified blocks that were changed by this query; while the number of blocks
\fIwritten\fR
indicates the number of previously\-dirtied blocks evicted from cache by this backend during query processing\&. The number of blocks shown for an upper\-level node includes those used by all its child nodes\&. In text format, only non\-zero values are printed\&. This parameter may only be used when
ANALYZE
is also enabled\&. It defaults to
FALSE\&.
.RE
.PP
TIMING
.RS 4
Include actual startup time and time spent in each node in the output\&. The overhead of repeatedly reading the system clock can slow down the query significantly on some systems, so it may be useful to set this parameter to
FALSE
when only actual row counts, and not exact times, are needed\&. Run time of the entire statement is always measured, even when node\-level timing is turned off with this option\&. This parameter may only be used when
ANALYZE
is also enabled\&. It defaults to
TRUE\&.
.RE
.PP
FORMAT
.RS 4
Specify the output format, which can be TEXT, XML, JSON, or YAML\&. Non\-text output contains the same information as the text output format, but is easier for programs to parse\&. This parameter defaults to
TEXT\&.
.RE
.PP
\fIboolean\fR
.RS 4
Specifies whether the selected option should be turned on or off\&. You can write
TRUE,
ON, or
1
to enable the option, and
FALSE,
OFF, or
0
to disable it\&. The
\fIboolean\fR
value can also be omitted, in which case
TRUE
is assumed\&.
.RE
.PP
\fIstatement\fR
.RS 4
Any
SELECT,
INSERT,
UPDATE,
DELETE,
VALUES,
EXECUTE,
DECLARE, or
CREATE TABLE AS
statement, whose execution plan you wish to see\&.
.RE
.PP
NODES
.RS 4
Include information on the Datanodes involved in the execution of Data Scan Node\&. This parameter defaults to
TRUE\&. This option is available in
Postgres\-XC\&.
.RE
.PP
NUM_NODES
.RS 4
Include information on the number of nodes involved in the execution of Data Node Scan node\&. This parameter defaults to
FALSE\&. This option is available in
Postgres\-XC\&.
.RE
.SH "OUTPUTS"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
The command\(aqs result is a textual description of the plan selected for the
\fIstatement\fR, optionally annotated with execution statistics\&.
Section 14.1, \(lqUsing EXPLAIN\(rq, in the documentation
describes the information provided\&.
.SH "NOTES"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
In order to allow the
PostgreSQL
query planner to make reasonably informed decisions when optimizing queries, the
pg_statistic
data should be up\-to\-date for all tables used in the query\&. Normally the
autovacuum daemon
will take care of that automatically\&. But if a table has recently had substantial changes in its contents, you might need to do a manual
\fBANALYZE\fR(7)
rather than wait for autovacuum to catch up with the changes\&.
.PP
In order to measure the run\-time cost of each node in the execution plan, the current implementation of
EXPLAIN ANALYZE
adds profiling overhead to query execution\&. As a result, running
EXPLAIN ANALYZE
on a query can sometimes take significantly longer than executing the query normally\&. The amount of overhead depends on the nature of the query, as well as the platform being used\&. The worst case occurs for plan nodes that in themselves require very little time per execution, and on machines that have relatively slow operating system calls for obtaining the time of day\&.
.SH "EXAMPLES"
.PP
To show the plan for a simple query on a table with a single
integer
column and 10000 rows:
.sp
.if n \{\
.RS 4
.\}
.nf
EXPLAIN SELECT * FROM foo;

                       QUERY PLAN
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
 Seq Scan on foo  (cost=0\&.00\&.\&.155\&.00 rows=10000 width=4)
(1 row)
.fi
.if n \{\
.RE
.\}
.PP
Here is the same query, with JSON output formatting:
.sp
.if n \{\
.RS 4
.\}
.nf
EXPLAIN (FORMAT JSON) SELECT * FROM foo;
           QUERY PLAN
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
 [                             +
   {                           +
     "Plan": {                 +
       "Node Type": "Seq Scan",+
       "Relation Name": "foo", +
       "Alias": "foo",         +
       "Startup Cost": 0\&.00,   +
       "Total Cost": 155\&.00,   +
       "Plan Rows": 10000,     +
       "Plan Width": 4         +
     }                         +
   }                           +
 ]
(1 row)
.fi
.if n \{\
.RE
.\}
.PP
If there is an index and we use a query with an indexable
WHERE
condition,
EXPLAIN
might show a different plan:
.sp
.if n \{\
.RS 4
.\}
.nf
EXPLAIN SELECT * FROM foo WHERE i = 4;

                         QUERY PLAN
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
 Index Scan using fi on foo  (cost=0\&.00\&.\&.5\&.98 rows=1 width=4)
   Index Cond: (i = 4)
(2 rows)
.fi
.if n \{\
.RE
.\}
.PP
Here is the same query, but in YAML format:
.sp
.if n \{\
.RS 4
.\}
.nf
EXPLAIN (FORMAT YAML) SELECT * FROM foo WHERE i=\(aq4\(aq;
          QUERY PLAN
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
 \- Plan:                      +
     Node Type: "Index Scan"  +
     Scan Direction: "Forward"+
     Index Name: "fi"         +
     Relation Name: "foo"     +
     Alias: "foo"             +
     Startup Cost: 0\&.00       +
     Total Cost: 5\&.98         +
     Plan Rows: 1             +
     Plan Width: 4            +
     Index Cond: "(i = 4)"
(1 row)
.fi
.if n \{\
.RE
.\}
.sp
XML format is left as an exercise for the reader\&.
.PP
Here is the same plan with cost estimates suppressed:
.sp
.if n \{\
.RS 4
.\}
.nf
EXPLAIN (COSTS FALSE) SELECT * FROM foo WHERE i = 4;

        QUERY PLAN
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
 Index Scan using fi on foo
   Index Cond: (i = 4)
(2 rows)
.fi
.if n \{\
.RE
.\}
.PP
Here is an example of a query plan for a query using an aggregate function:
.sp
.if n \{\
.RS 4
.\}
.nf
EXPLAIN SELECT sum(i) FROM foo WHERE i < 10;

                             QUERY PLAN
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
 Aggregate  (cost=23\&.93\&.\&.23\&.93 rows=1 width=4)
   \->  Index Scan using fi on foo  (cost=0\&.00\&.\&.23\&.92 rows=6 width=4)
         Index Cond: (i < 10)
(3 rows)
.fi
.if n \{\
.RE
.\}
.PP
Here is an example of using
EXPLAIN EXECUTE
to display the execution plan for a prepared query:
.sp
.if n \{\
.RS 4
.\}
.nf
PREPARE query(int, int) AS SELECT sum(bar) FROM test
    WHERE id > $1 AND id < $2
    GROUP BY foo;

EXPLAIN ANALYZE EXECUTE query(100, 200);

                                                       QUERY PLAN
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
 HashAggregate  (cost=39\&.53\&.\&.39\&.53 rows=1 width=8) (actual time=0\&.661\&.\&.0\&.672 rows=7 loops=1)
   \->  Index Scan using test_pkey on test  (cost=0\&.00\&.\&.32\&.97 rows=1311 width=8) (actual time=0\&.050\&.\&.0\&.395 rows=99 loops=1)
         Index Cond: ((id > $1) AND (id < $2))
 Total runtime: 0\&.851 ms
(4 rows)
.fi
.if n \{\
.RE
.\}
.PP
Of course, the specific numbers shown here depend on the actual contents of the tables involved\&. Also note that the numbers, and even the selected query strategy, might vary between
PostgreSQL
releases due to planner improvements\&. In addition, the
ANALYZE
command uses random sampling to estimate data statistics; therefore, it is possible for cost estimates to change after a fresh run of
ANALYZE, even if the actual distribution of data in the table has not changed\&.
.SH "COMPATIBILITY"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
There is no
EXPLAIN
statement defined in the SQL standard\&.
.SH "SEE ALSO"
\fBANALYZE\fR(7)
