'\" t
.\"     Title: PREPARE
.\"    Author: The Postgres-XC Development Group
.\" Generator: DocBook XSL Stylesheets v1.75.2 <http://docbook.sf.net/>
.\"      Date: 2014
.\"    Manual: Postgres-XC 1.2.1 Documentation
.\"    Source: Postgres-XC 1.2.1
.\"  Language: English
.\"
.TH "PREPARE" "7" "2014" "Postgres-XC 1.2.1" "Postgres-XC 1.2.1 Documentation"
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
PREPARE \- prepare a statement for execution
.\" PREPARE
.\" prepared statements: creating
.SH "SYNOPSIS"
.sp
.nf
PREPARE \fIname\fR [ ( \fIdata_type\fR [, \&.\&.\&.] ) ] AS \fIstatement\fR
.fi
.SH "DESCRIPTION"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
PREPARE
creates a prepared statement\&. A prepared statement is a server\-side object that can be used to optimize performance\&. When the
PREPARE
statement is executed, the specified statement is parsed, analyzed, and rewritten\&. When an
EXECUTE
command is subsequently issued, the prepared statement is planned and executed\&. This division of labor avoids repetitive parse analysis work, while allowing the execution plan to depend on the specific parameter values supplied\&.
.PP
Prepared statements can take parameters: values that are substituted into the statement when it is executed\&. When creating the prepared statement, refer to parameters by position, using
$1,
$2, etc\&. A corresponding list of parameter data types can optionally be specified\&. When a parameter\(aqs data type is not specified or is declared as
unknown, the type is inferred from the context in which the parameter is used (if possible)\&. When executing the statement, specify the actual values for these parameters in the
EXECUTE
statement\&. Refer to
\fBEXECUTE\fR(7)
for more information about that\&.
.PP
Prepared statements only last for the duration of the current database session\&. When the session ends, the prepared statement is forgotten, so it must be recreated before being used again\&. This also means that a single prepared statement cannot be used by multiple simultaneous database clients; however, each client can create their own prepared statement to use\&. Prepared statements can be manually cleaned up using the
\fBDEALLOCATE\fR(7)
command\&.
.PP
Prepared statements have the largest performance advantage when a single session is being used to execute a large number of similar statements\&. The performance difference will be particularly significant if the statements are complex to plan or rewrite, for example, if the query involves a join of many tables or requires the application of several rules\&. If the statement is relatively simple to plan and rewrite but relatively expensive to execute, the performance advantage of prepared statements will be less noticeable\&.
.SH "PARAMETERS"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
\fIname\fR
.RS 4
An arbitrary name given to this particular prepared statement\&. It must be unique within a single session and is subsequently used to execute or deallocate a previously prepared statement\&.
.RE
.PP
\fIdata_type\fR
.RS 4
The data type of a parameter to the prepared statement\&. If the data type of a particular parameter is unspecified or is specified as
unknown, it will be inferred from the context in which the parameter is used\&. To refer to the parameters in the prepared statement itself, use
$1,
$2, etc\&.
.RE
.PP
\fIstatement\fR
.RS 4
Any
SELECT,
INSERT,
UPDATE,
DELETE, or
VALUES
statement\&.
.RE
.SH "NOTES"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
If a prepared statement is executed enough times, the server may eventually decide to save and re\-use a generic plan rather than re\-planning each time\&. This will occur immediately if the prepared statement has no parameters; otherwise it occurs only if the generic plan appears to be not much more expensive than a plan that depends on specific parameter values\&. Typically, a generic plan will be selected only if the query\(aqs performance is estimated to be fairly insensitive to the specific parameter values supplied\&.
.PP
To examine the query plan
PostgreSQL
is using for a prepared statement, use
\fBEXPLAIN\fR(7)\&. If a generic plan is in use, it will contain parameter symbols
$\fIn\fR, while a custom plan will have the current actual parameter values substituted into it\&.
.PP
For more information on query planning and the statistics collected by
PostgreSQL
for that purpose, see the
\fBANALYZE\fR(7)
documentation\&.
.PP
Although the main point of a prepared statement is to avoid repeated parse analysis and planning of the statement,
PostgreSQL
will force re\-analysis and re\-planning of the statement before using it whenever database objects used in the statement have undergone definitional (DDL) changes since the previous use of the prepared statement\&. Also, if the value of
search_path
changes from one use to the next, the statement will be re\-parsed using the new
\fIsearch_path\fR\&. (This latter behavior is new as of
PostgreSQL
9\&.3\&.) These rules make use of a prepared statement semantically almost equivalent to re\-submitting the same query text over and over, but with a performance benefit if no object definitions are changed, especially if the best plan remains the same across uses\&. An example of a case where the semantic equivalence is not perfect is that if the statement refers to a table by an unqualified name, and then a new table of the same name is created in a schema appearing earlier in the
\fIsearch_path\fR, no automatic re\-parse will occur since no object used in the statement changed\&. However, if some other change forces a re\-parse, the new table will be referenced in subsequent uses\&.
.PP
You can see all prepared statements available in the session by querying the
pg_prepared_statements
system view\&.
.SH "EXAMPLES"
.PP
Create a prepared statement for an
INSERT
statement, and then execute it:
.sp
.if n \{\
.RS 4
.\}
.nf
PREPARE fooplan (int, text, bool, numeric) AS
    INSERT INTO foo VALUES($1, $2, $3, $4);
EXECUTE fooplan(1, \(aqHunter Valley\(aq, \(aqt\(aq, 200\&.00);
.fi
.if n \{\
.RE
.\}
.PP
Create a prepared statement for a
SELECT
statement, and then execute it:
.sp
.if n \{\
.RS 4
.\}
.nf
PREPARE usrrptplan (int) AS
    SELECT * FROM users u, logs l WHERE u\&.usrid=$1 AND u\&.usrid=l\&.usrid
    AND l\&.date = $2;
EXECUTE usrrptplan(1, current_date);
.fi
.if n \{\
.RE
.\}
.sp
Note that the data type of the second parameter is not specified, so it is inferred from the context in which
$2
is used\&.
.SH "COMPATIBILITY"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
The SQL standard includes a
PREPARE
statement, but it is only for use in embedded SQL\&. This version of the
PREPARE
statement also uses a somewhat different syntax\&.
.SH "SEE ALSO"
\fBDEALLOCATE\fR(7), \fBEXECUTE\fR(7)
