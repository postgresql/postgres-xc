'\" t
.\"     Title: ANALYZE
.\"    Author: The Postgres-XC Development Group
.\" Generator: DocBook XSL Stylesheets v1.75.2 <http://docbook.sf.net/>
.\"      Date: 2014
.\"    Manual: Postgres-XC 1.2.1 Documentation
.\"    Source: Postgres-XC 1.2.1
.\"  Language: English
.\"
.TH "ANALYZE" "7" "2014" "Postgres-XC 1.2.1" "Postgres-XC 1.2.1 Documentation"
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
ANALYZE \- collect statistics about a database
.\" ANALYZE
.SH "SYNOPSIS"
.sp
.nf
ANALYZE [ VERBOSE ] [ \fItable_name\fR [ ( \fIcolumn_name\fR [, \&.\&.\&.] ) ] ]
.fi
.SH "DESCRIPTION"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
ANALYZE
collects statistics about the contents of tables in the database, and stores the results in the
pg_statistic
system catalog\&. Subsequently, the query planner uses these statistics to help determine the most efficient execution plans for queries\&.
.PP
With no parameter,
ANALYZE
examines every table in the current database\&. With a parameter,
ANALYZE
examines only that table\&. It is further possible to give a list of column names, in which case only the statistics for those columns are collected\&.
.SH "PARAMETERS"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
VERBOSE
.RS 4
Enables display of progress messages\&.
.RE
.PP
\fItable_name\fR
.RS 4
The name (possibly schema\-qualified) of a specific table to analyze\&. If omitted, all regular tables (but not foreign tables) in the current database are analyzed\&.
.RE
.PP
\fIcolumn_name\fR
.RS 4
The name of a specific column to analyze\&. Defaults to all columns\&.
.RE
.SH "OUTPUTS"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
When
VERBOSE
is specified,
ANALYZE
emits progress messages to indicate which table is currently being processed\&. Various statistics about the tables are printed as well\&.
.SH "NOTES"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
Foreign tables are analyzed only when explicitly selected\&. Not all foreign data wrappers support
ANALYZE\&. If the table\(aqs wrapper does not support
ANALYZE, the command prints a warning and does nothing\&.
.PP
In the default
PostgreSQL
configuration, the autovacuum daemon (see
Section 22.1.6, \(lqThe Autovacuum Daemon\(rq, in the documentation) takes care of automatic analyzing of tables when they are first loaded with data, and as they change throughout regular operation\&. When autovacuum is disabled, it is a good idea to run
ANALYZE
periodically, or just after making major changes in the contents of a table\&. Accurate statistics will help the planner to choose the most appropriate query plan, and thereby improve the speed of query processing\&. A common strategy for read\-mostly databases is to run
\fBVACUUM\fR(7)
and
ANALYZE
once a day during a low\-usage time of day\&. (This will not be sufficient if there is heavy update activity\&.)
.PP
ANALYZE
requires only a read lock on the target table, so it can run in parallel with other activity on the table\&.
.PP
The statistics collected by
ANALYZE
usually include a list of some of the most common values in each column and a histogram showing the approximate data distribution in each column\&. One or both of these can be omitted if
ANALYZE
deems them uninteresting (for example, in a unique\-key column, there are no common values) or if the column data type does not support the appropriate operators\&. There is more information about the statistics in
Chapter 22, Routine Database Maintenance Tasks, in the documentation\&.
.PP
For large tables,
ANALYZE
takes a random sample of the table contents, rather than examining every row\&. This allows even very large tables to be analyzed in a small amount of time\&. Note, however, that the statistics are only approximate, and will change slightly each time
ANALYZE
is run, even if the actual table contents did not change\&. This might result in small changes in the planner\(aqs estimated costs shown by
\fBEXPLAIN\fR(7)\&. In rare situations, this non\-determinism will cause the planner\(aqs choices of query plans to change after
ANALYZE
is run\&. To avoid this, raise the amount of statistics collected by
ANALYZE, as described below\&.
.PP
The extent of analysis can be controlled by adjusting the
default_statistics_target
configuration variable, or on a column\-by\-column basis by setting the per\-column statistics target with
ALTER TABLE \&.\&.\&. ALTER COLUMN \&.\&.\&. SET STATISTICS
(see
ALTER TABLE (\fBALTER_TABLE\fR(7)))\&. The target value sets the maximum number of entries in the most\-common\-value list and the maximum number of bins in the histogram\&. The default target value is 100, but this can be adjusted up or down to trade off accuracy of planner estimates against the time taken for
ANALYZE
and the amount of space occupied in
pg_statistic\&. In particular, setting the statistics target to zero disables collection of statistics for that column\&. It might be useful to do that for columns that are never used as part of the
WHERE,
GROUP BY, or
ORDER BY
clauses of queries, since the planner will have no use for statistics on such columns\&.
.PP
The largest statistics target among the columns being analyzed determines the number of table rows sampled to prepare the statistics\&. Increasing the target causes a proportional increase in the time and space needed to do
ANALYZE\&.
.PP
One of the values estimated by
ANALYZE
is the number of distinct values that appear in each column\&. Because only a subset of the rows are examined, this estimate can sometimes be quite inaccurate, even with the largest possible statistics target\&. If this inaccuracy leads to bad query plans, a more accurate value can be determined manually and then installed with
ALTER TABLE \&.\&.\&. ALTER COLUMN \&.\&.\&. SET (n_distinct = \&.\&.\&.)
(see
ALTER TABLE (\fBALTER_TABLE\fR(7)))\&.
.PP
If the table being analyzed has one or more children,
ANALYZE
will gather statistics twice: once on the rows of the parent table only, and a second time on the rows of the parent table with all of its children\&. This second set of statistics is needed when planning queries that traverse the entire inheritance tree\&. The autovacuum daemon, however, will only consider inserts or updates on the parent table itself when deciding whether to trigger an automatic analyze for that table\&. If that table is rarely inserted into or updated, the inheritance statistics will not be up to date unless you run
ANALYZE
manually\&.
.PP
If the table being analyzed is completely empty,
ANALYZE
will not record new statistics for that table\&. Any existing statistics will be retained\&.
.SH "COMPATIBILITY"
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
.PP
The following description applies both to
Postgres\-XC
and
PostgreSQL
if not described explicitly\&. You can read
PostgreSQL
as
Postgres\-XC
except for version number, which is specific to each product\&.
.sp .5v
.RE
.PP
There is no
ANALYZE
statement in the SQL standard\&.
.SH "SEE ALSO"
\fBVACUUM\fR(7), \fBvacuumdb\fR(1), Section 17.4.4, \(lqCost-based Vacuum Delay\(rq, in the documentation, Section 22.1.6, \(lqThe Autovacuum Daemon\(rq, in the documentation
